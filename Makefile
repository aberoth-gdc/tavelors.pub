run: test
	pipenv run python main.py

test:
	pipenv run python -m pytest --cov-report term-missing --cov tavelor tests/unit/test_database.py

.PHONY: run, test
