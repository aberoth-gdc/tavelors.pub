"""
Main runner for all Discord bots. Optimized to allow bots to interact with
eachother and share interaction data.
"""

import asyncio
import logging
import os

import discord
import quart

import lysis
import darklow
import tavelor

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

app = quart.Quart(__name__)

LYSIS_TOKEN: str = os.getenv('LYSIS_TOKEN')
TAVELOR_TOKEN: str = os.getenv('TAVELOR_TOKEN')
INALA_TOKEN: str = os.getenv('INALA_TOKEN')
DARKLOW_TOKEN: str = os.getenv('DARKLOW_TOKEN')

@app.route("/api", methods=["GET"])
async def hello():
	return "Hello world!\n", 200

loop = asyncio.get_event_loop()
loop.create_task(lysis.client.start(LYSIS_TOKEN))
loop.create_task(darklow.client.start(DARKLOW_TOKEN))
#loop.create_task(tavelor.client.start(TAVELOR_TOKEN))
#loop.create_task(inala.client.start(INALA_TOKEN))
PORT = os.getenv("PORT")
loop.create_task(app.run_task('0.0.0.0', PORT))
loop.run_forever()
