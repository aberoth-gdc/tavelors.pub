"""
Utility functions for the Tavelor Discord bot
"""

import logging
import re

banned_words = [
    # Homophobic slurs
    "faggot",
    # Anti-semetic
    "kike",
    "jew",
    # Racism
    #  Anti-black
    "nigger",
    "niqqer",
    "nigga",
    "niqqa",
    "nig",
    "niq",
    #  Anti-asian
    "spic",
    "chink",
    #  Anti-hispanic
    "beaner",
    "wetback",
    # Reserved
    "tavelor"
]

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

discord_id = re.compile(r'^(?P<o><)?(@!?)?(?P<id>\d{15,20})(?(o)>|)$')

def is_name(name):
    """
    Checks if the name is a valid Aberoth username.
    """
    return (
        isinstance(name, str)
        and name.isalpha()
        and len(name) >=3
        and len(name) <=10
        and name not in banned_words
    )


def is_id(ident):
    """
    Checks if the given string is an identity we support.
    """
    return is_name(ident) or is_discord(ident)


def is_discord(ident):
    """
    Checks if the input is a valid Discord mention/id
    """
    return (
        (
            isinstance(ident, int)
            # TODO: Get a better test
            and ident >= 100000000000000
        ) or (
            isinstance(ident, str) and discord_id.match(ident) is not None
        )
    )


def get_discord_id(ident):
    """
    Returns the integer value Discord identity
    """
    logger.debug("Getting ID from {}".format(ident))

    if isinstance(ident, int) and 1000000000000000000 > ident >= 100000000000000000:
        logger.debug("Passed an int, returning it as is")
        return ident
    if not is_discord(ident):
        logger.debug("Not a discord ID: {}".format(ident))
        return 0
    logger.debug("Returning {}".format(discord_id.match(ident).groupdict()["id"]))

    return int(discord_id.match(ident).groupdict()["id"])


def preprocess_input(inp):
    """
    Runs a client input through a series of checks and changes.
    """
    output = []
    changed = False

    if isinstance(inp, str):
        inp = inp.split()

    for i, word in enumerate(inp):
        if isinstance(word, (float,int)):
            output.append(word)
            continue
        word = word.lower().replace('?', '')
        if is_discord(word):
            output.append(get_discord_id(word))
        elif word in ("who", "who's", "whos"):
            output.append("who")
        # Since we shorten who, get rid of the is
        elif word == "is":
            if output[i-1] in ("who", "else"):
                continue
            output.append(word)
        elif word == "":
            continue
        elif word in ("a", "an"):
            output.append("a")
        elif word in ("anymore", "now"):
            changed = True
            continue
        else:
            output.append(word)

    if changed:
        output.append("now")

    return output
