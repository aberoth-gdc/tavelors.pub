"""
Functions for retreiving information from the Aberoth.com website
"""

from urllib.request import urlopen

from bs4 import BeautifulSoup

from .models import Guild, Champion


MOST_SKILLFUL = 'https://aberoth.com/highscore/Most_Skillful.html'
#MOST_SKILLFUL = 'https://aberoth.com/highscore/Most_Skillful_More.html'
GUILDS = 'https://aberoth.com/highscore/Guilds.html'
WEALTHIEST = ""
NAME_CHANGES = ""


def scrape(url):
    """
    Generic function to grab a table from a webpage.
    """
    # Open the url and parse the html
    html = urlopen(url)
    soup = BeautifulSoup(html, 'html.parser')
    # extract the first table
    table = soup.find_all('table')[0]
    rows = table.find_all('tr')

    result = []
    for row in rows:
        filtered_row = []
        for cell in row.find_all(['td']):
            filtered_row.append(cell.get_text())
        result.append(filtered_row)

    return result


def scrape_guilds():
    """
    Scrapes the most recent guild information from the highscore website.
    """
    return scrape(GUILDS)


def scrape_champions():
    """
    Scrapes the most recent Champion information from the highscore page.
    """
    return scrape(MOST_SKILLFUL)


def scrape_namechanges():
    """
    Scrapes the most recent name change information from the highscore page.
    """
    raise NotImplementedError()
