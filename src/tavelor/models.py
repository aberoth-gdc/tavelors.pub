"""
Classes used in the Tavelor Discord bot.
"""

import logging

from sqlalchemy import Column, Integer, Sequence, String
from sqlalchemy.orm import declarative_base

from .util import is_name
from .errors import InvalidNameError

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

ABJURATION = "Abjuration"
CONJURATION = "Conjuration"
DIVINATION = "Divination"
ENCHANTMENT = "Enchantment"
EVOCATION = "Evocation"
ILLUSION = "Illusion"
NECROMANCY = "Necromancy"
TRANSMUTATION = "Transmutation"

schools = {
    "": "",
    "abj": ABJURATION,
    "abjuration": ABJURATION,
    "conj": CONJURATION,
    "conjuration": CONJURATION,
    "div": DIVINATION,
    "divination": DIVINATION,
    "ench": ENCHANTMENT,
    "enchantment": ENCHANTMENT,
    "evo": EVOCATION,
    "evocation": EVOCATION,
    "illu": ILLUSION,
    "illusion": ILLUSION,
    "necro": NECROMANCY,
    "necromancy": NECROMANCY,
    "trans": TRANSMUTATION,
    "transmutation": TRANSMUTATION,
}


Base = declarative_base()


class Identity():
    """
    Object representing an identifyable person, who may have Aberoth accounts
    that are traded, and preferrably has a Discord.
    """

    __tablename__ = 'identities'

    id = Column(
        Integer, 
        Sequence('user_id_seq'),
        primary_key=True,
        nullable=False
    )
    discord = Column(String)
    guild = Column(String)
    alias = Column(String)
    alts = Column(String)

    def __repr__(self):
        return "<Identity(discord='%s', guild='%s', alias='%s')>" % (
            self.discord, self.guild, self.alias)

    # TODO: Make all fields optional
    def __init__(self, og, discord, guild, status, alts):
        if not is_name(og):
            logger.error("InvalidNameError({})".format(og))
            raise InvalidNameError(og)

        if isinstance(alts, str):
            alts = alts.split("|")

        for alt in alts:
            if alt == "":
                logger.error(
                    "Encountered blank alt name for Identity {} ({})".format(
                        og, discord
                    )
                )
                continue
            if not is_name(alt):
                raise InvalidNameError(alt)

        self.alias = og.lower()
        self.discord = discord
        self.guild = guild
        self.status = status
        self.alts = [alt.lower() for alt in alts]


    def asdict(self):
        """
        TODO
        """
        result = self.__dict__
        result["alts"] = "|".join(self.alts)
        return result


class Guild():
    """
    Object representing a Guild and allegiance information.
    """

    __tablename__ = "guilds"

    def __init__(self, name, leader="", skill_total=0, size=0):
        self.name = name
        self.leader = leader
        self.skill_total = skill_total
        self.size = size

    def get_leader(self):
        """
        TODO
        """
        return self.leader

    def get_average_skill(self):
        """
        Returns the average skill of users in this Guild.
        """
        return self.skill_total / self.size


class Alliance():
    """
    Object representing a set of Guilds allied to one another.
    """

    __tablename__ = "alliances"

    def __init__(self, name, leader=None):
        self.name = name
        self.leader = leader
        self.guilds = []


    def add_guild(self, guild):
        """
        Adds a guild to this list.
        """
        # TODO: Can guilds be in more than one alliance?
        if guild.alliance is not None:
            if guild.alliance.name == self.name:
                return
            raise Exception("Guild cannot be in more than one alliance.")

        guild.alliance = self
        self.guilds.append(guild)


    def remove_guild(self, guild):
        """
        Removes a guild from the list.
        """

        if guild.alliance is None:
            return

        if guild.alliance.name != self.name:
            raise Exception("Guild is not in this alliance.")

        guild.alliance = None
        self.guilds.remove(guild)


class Champion():
    """
    Object representing one Aberoth champion.
    """

    __tablename__ = "champions"

    name = Column(String(10))
    skill_total = Column(Integer())
    school = Column(String())
    guild = Column(String())

    def __init__(self, name, skill_total=0, school="", guild=None,
                 identity=None):
        if not is_name(name):
            logger.error("InvalidNameError({})".format(name))
            raise InvalidNameError(name)
        #assert school.lower() in schools
        self.name = name.lower()
        self.skill_total=skill_total
        self.school=schools[school.lower()]
        self.guild=guild
        self.identity=identity
