"""
All modules for running a Tavelor Discord bot.
"""

import logging

import discord

from .database import load_db, identities
from .scrape import scrape_champions, scrape_guilds

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

client = discord.Client()
