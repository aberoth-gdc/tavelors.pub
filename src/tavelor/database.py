"""
Functions for reading and writing from the databases.
"""

import csv
import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base

from .models import Identity, Guild, Champion, Base
from .util import is_id, is_discord, get_discord_id
from .errors import DuplicateDiscordError
from .scrape import scrape_guilds, scrape_champions

BUCKET = ""

# TODO: Figure out a better way to do this
FRIENDLY = "allied"
ENEMY = "hostile"
NEUTRAL = "neutral"

# postgresql+psycopg2://postgres:password@localhost:5432/yohouse-yodismo

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.INFO)

champions = {}
identities = {}
guilds = {}
alliances = {}

# Filenames for our various databases
IDENTITY_FILE="identities.csv"
GUILDS_FILE = "guilds.csv"
CHAMPIONS_FILE = "champions.csv"
ALLIANCE_FILE = "alliances.csv"


# Headers for the various database CSV files
IDENTITY_FIELDS = ["guild", "status", "discord", "alias", "alts"]
CHAMPION_FIELDS = ["name", "guild", "identity", "school"]
GUILD_FIELDS = ["name", "status", "realm", "alliance", "members", "champions", "is_active"]
ALLIANCE_FIELDS = ["name", "leader", "guilds", "status", "is_active"]


def get_identities_list():
    """
    Returns a list of the identities we're tracking.
    """
    return list(set(identities.values()))


def get_champions_list():
    """
    Returns a list of the champions we're tracking.
    """
    return list(set(champions.values()))


def get_guilds_list():
    """
    Returns a list of the guilds we're tracking.
    """
    return list(set(guilds.values()))


def load_s3_db():
    """
    Gets the database from a remote S3 bucket.
    """
    raise NotImplementedError()


def write_s3_db():
    """
    Writes the database to a remote S3 bucket.
    """
    raise NotImplementedError()


def load_identities():
    """
    Loads the saved list of alts from a database.
    Returns a dictionary for easy user lookup.
    """

    if BUCKET != "":
        return load_s3_db()

    with open(IDENTITY_FILE, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        # Skip the header
        next(reader)

        count_alts = 0
        for row in reader:
            logger.debug("Read row: {}".format(row))
            # guild, status, discord, alias, alts
            # self, alias, discord, guild, status, alts):
            identity = Identity(row[3],row[2],row[0],row[1],row[4].split("|"))

            if int(identity.discord) != 0:
                if identity.discord in identities:
                    raise DuplicateDiscordError(identity.discord)
                identities[identity.discord] = identity
            for alt in identity.alts:
                count_alts += 1
                identities[alt] = identity

    logger.info("We know about {} unique Identities.".format(
        len(get_identities_list())
    ))
    logger.info("We know the Identity of {} Champions.".format(count_alts))
    return identities


def load_guilds():
    """
    TODO
    """

    logger.debug("Loading Guilds")

    for row in scrape_guilds():
        guild = Guild(
            row[1],
            leader=row[2],
            skill_total=row[3],
            size=row[4]
        )

        guilds[guild.name.lower()] = guild

    logger.info("Loaded {} guilds.".format(len(get_guilds_list())))

    return guilds


def load_champions_csv():
    """
    Loads all known champions from the stored CSV, then compares to a scrae from
    the Aberoth leaderboards
    """

    logger.debug("Loading Champions from CSV")
    with open(CHAMPIONS_FILE, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        # Skip the header
        next(reader)

        # Let's count how many champions we know the school of
        school_count = 0
        for row in reader:
            #CHAMPION_FIELDS = ["name", "guild", "identity", "school"]
            if row[3] != "":
                school_count += 1
            champion = Champion(row[0],guild=row[1],identity=row[2],school=row[3])
            champions[champion.name.lower()] = champion

    logger.info("We know the school of {} Champions".format(school_count))

    for row in scrape_champions():
        # Skip the header
        if row[1].strip() == "Name":
            continue

        # Skip bad names idc
        if row[1] in ("N---o", "S--------r", "K-------r", "N----r", "R-------n",
                      "M-------e"):
            continue

        if row[1].lower() in champions:
            champion = champions[row[1].lower()]
            champion.skill_total=int(row[2])
        else:
            champion = Champion(
                row[1],
                skill_total=int(row[2])
            )

            champions[champion.name.lower()] = champion

    logger.info("Loaded {} champions.".format(len(get_champions_list())))

    return champions


def load_db():
    """
    TODO
    """
    _load_db_csv()


def _load_db_csv():
    """
    Loads all the information
    TODO: Better description
    """

    load_champions_csv()
    load_guilds()
    load_identities()

    #fix_connections()

    return identities, guilds, champions


def _load_db_postgres():
    """
    TODO
    """
    raise NotImplementedError


def fix_connections():
    for identity in get_identities_list():
        for alt in identity.alts:
            if int(identity.discord) != 0:
                champions[alt].identity = identity.discord
            else:
                champions[alt].identity = identity.alias


def write(file, fields, data, to_dict):
    with open(file, 'w') as f:
        writer =  csv.DictWriter(f, fieldnames=fields)
        writer.writeheader()
        logger.info("Writing {} rows.".format(len(data)))
        for row in data:
            as_dict = to_dict(row)
            writer.writerow(as_dict)


def write_identities():
    """
    TODO
    """
    write(IDENTITY_FILE, IDENTITY_FIELDS, get_identities_list(),
          lambda identity: {
                "guild": identity.guild,
                "status": identity.status,
                "discord": int(identity.discord),
                "alias": identity.alias,
                "alts": "|".join(identity.alts)
            })

def write_champions():
    """
    TODO
    """
    write(CHAMPIONS_FILE, CHAMPION_FIELDS, get_champions_list(),
          lambda champion: {
              "name": champion.name,
              "guild": champion.guild,
              "identity": champion.identity,
              "school": champion.school,
          })


def write_db():
    """
    Converts our objects into CSV files.
    """
    write_identities()
    write_champions()
    logger.info("Writing database")


_engine = None
def get_db_engine():
    """
    SQLAlchemy engine singleton
    """
    if _engine is None:
        _engine = create_engine("postgresql+psycopg2://postgres:password@localhost:5432/yohouse-yodismo", echo=True, future=True)
    return _engine


_session = None
def get_db_session():
    """
    SQLAlchemy ORM Session singleton
    """
    _engine = get_db_engine()

    if _session is None:
        Session = sessionmaker(bind = _engine)
        _session = Session()


def init_db() -> None:
    """
    Performs migrations, etc, on the connected database.
    """
    Base.metadata.create_all(get_db_engine())
