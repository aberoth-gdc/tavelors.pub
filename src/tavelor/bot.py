"""
Functions relating to the bot.
"""

trivia: dict[str, str] = {
    "who": "Come find me in my tavern, {}, I'll have more to say there.",
    "elsewhere": "Come find me in my tavern, {}, I'll have more to say there.",
    "help": "I currently have no help documents, please ping Twoshields.",
    "coffee": "Beer?",
    "inala": "Inala has no idea how important her work is.",
    "sholop": "Sholop can be a bit thick at times.",
}
