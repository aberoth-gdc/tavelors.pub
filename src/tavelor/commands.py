"""
Commands that Tavelor bot uses to give responses.
"""

import logging

from .database import get_identities_list, get_guilds_list
from .database import identities, guilds, champions
from .errors import InvalidIdError, UnknownIdentityError
from .models import Identity, schools
from .util import is_discord, get_discord_id, is_id, is_name

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def add_user(first: str, second: str, message) -> str:
    """
    TODO
    """
    discord: int = 0
    alts: list[str] = []
    patron = message.author.mention
    identity: Identity = None
    logger.info("add_user {} {}".format(first, second))

    if first == second:
        return "What a silly thing to say, {}!".format(patron)

    if is_discord(first) and is_discord(second):
        # TODO: Handle double Discords, for spies or something
        return "I can't keep all that in my head. Who's who?"

    # Now we need to figure out if we were given a Discord at all

    # If the OG is a Discord username, corrent our variables
    if is_discord(first):
        logger.debug("First argument was a discord user.")
        discord = get_discord_id(first)
        # Make sure we know the first input was a Discord ID
        first = ""
    else:
        alts.append(first)

    if is_discord(second):
        logger.debug("Second argument was a discord user.")
        discord = get_discord_id(second)
        # Make sure we know the second input was a Discord ID
        second = ""
    else:
        # If the OG was a discord username, we treat the alt as OG
        if first == "":
            first = second
            second = ""

        alts.append(second)

    msg: str = "Thanks, {}. I'll keep that in mind.".format(patron)

    assert isinstance(discord, int)

    # Check Discord first, since we give that preference
    if discord != 0 and discord in identities:
        identity = identities[discord]
        logger.info(
            "Found matching identity from discord: {}".format(identity.og)
        )

        if first in identity.alts:
            logger.debug("a I have {} identities.".format(
                len(get_identities_list())
            ))
            return "I think I already know that, {}".format(patron)

    if first in identities:
        if identity is not None:
            identity = identities[first]
        elif first not in identity.alts and first != identity.og:
            logger.debug("b I have {} users.".format(
                len(get_identities_list())
            ))
            return "I think their name is <@!{}>, {}".format(
                identity.discord,
                patron
            )

        # We've heard of the first, and we've heard of the second
        if second != "" and second in identities:
            # But they're different people
            if identity.og != identities[second].og:
                return "I think those are different people, {}".format(patron)

            return "I think I already know that, {}".format(patron)

    # We've never heard of first but we've heard of second
    if second != "" and second in identities:
        # first should be the one we know about, second is the addition
        first, second = (second, first)
        identity = identities[first]
    else:
        identity = Identity(first,discord,'','',alts)
        logger.debug("Adding user {}".format(identity))
        msg = "{} I've not heard of {} before..".format(msg, identity.alias)

    if first != "" and first not in identity.alts:
        identity.alts.append(first)

    if second != "" and second not in identity.alts:
        identity.alts.append(second)

    if int(discord) != 0 and int(identity.discord) == 0:
        identity.discord = discord

    if int(identity.discord) != 0:
        identities[identity.discord] = identity

    for alt in identity.alts:
        identities[alt] = identity

    logger.debug("Now have {} users.".format(len(get_identities_list())))

    return msg


# TODO: Need to find a way to NOT pass the discord client in...
async def identify(name, mention):
    """
    Returns a message giving info on the Identity of a name.
    """
    if not is_name(name):
        return "I only know citizens of Aberoth, {}".format(mention)

    if name not in identities:
        return "I don't know who {} is.".format(name.capitalize())

    identity = identities[name]

    if int(identity.discord) == 0:
        return "{} is {}.".format(name.capitalize(), identity.og.capitalize())

    msg = "{} is {}.".format(name.capitalize(), "<@!{}>".format(identity.discord))

    if identity.guild != "":
        if guilds[identity.guild.lower()].leader.lower() in identity.alts:
            msg = "{} They are the leader of {}.".format(msg, identity.guild)
        else:
            msg = "{} They are a member of {}.".format(msg, identity.guild)

    return msg


def set_guild(name, guild_name, message):
    """
    Sets the guild affiliation of an Identity.
    """
    logger.debug("There are {} guilds loaded.".format(len(get_guilds_list())))
    if not is_id(name):
        raise InvalidIdError(name)
    if guild_name not in guilds:
        logger.warning("{} not in guilds".format(guild_name))
        return "I'm not sure I've heard of that guild, {}".format(mention)

    mention = message.author.mention

    guild = guilds[guild_name]

    if name not in identities:
        # TODO: Is there anything we can do here?
        return  "I'm not sure I've heard that name before, {}".format(mention)

    identity = identities[name]

    if identity.guild == guild.name:
        return "I think I already knew that, {}".format(mention)

    identity.guild = guild.name

    msg = "Thank you for telling me, {}.".format(mention)

    return msg

    #if guild.status == FRIENDLY:
    #    if identity.status == ENEMY:
    #        msg = "{} However, I'd still keep my eye on them..".format(msg)
    #    elif identity.status == FRIENDLY:
    #        msg = "{} It's been a long time coming for this.".format(msg)
    #    else:
    #        msg = "{} It's always nice to make new friends.".format(msg)
    #    return msg
    #if guild.status == ENEMY:
    #    if identity.status == FRIENDLY:
    #        return "{} It's a shame, I had high hopes for them.".format(msg)
    #    if identity.status == NEUTRAL:
    #        return "{} It's a shame, I hope they know what they're getting into..".format(msg)
    #    if identity.status == ENEMY:
    #        return "{} It was something I had seen coming. So be it.".format(msg)


def set_og(name, og, mention):
    if og == name:
        return "I already know that, {}".format(mention)

    if name not in identities:
        msg = add_user(name, "", mention)

    identity = identities[name]
    identity.og = og

    msg = "Thank you, {}, I'll keep that in mind.".format(mention)

    ident_og = identities[og] if og in identities else None
    if ident_og is not None and ident_og.main != identity.main:
        if ident_og.og == og:
            # TODO: What to do here?
            ident_og.og = ""
        return "{} It's a shame they gave that account to {}.".format(
            msg,
            ident_og.discord if ident_og.discord != 0 else ident_og.main
        )

    return msg

def set_ident_status(name, status, mention):
    """
    Sets the status of an Identity
    """
    raise NotImplementedError()


def set_guild_alliance(name, alliance_name, mention, change=False):
    """
    Adds a guild to an alliance
    """
    raise NotImplementedError()


def get_alts(ident, mention):
    """
    Returns a message about the Identity's other Champions
    """
    logger.debug("get alts {} ({}) {} ({})".format(ident, type(ident), mention,
                                                   type(mention)))

    logger.debug("is id({}): {}".format(ident, is_id(ident)))
    if not is_id(ident):
        logger.debug("is id({}): {}".format(ident, is_id(ident)))
        raise InvalidIdError(ident)

    if ident not in identities:
        raise UnknownIdentityError(ident)

    identity = identities[ident]

    alts = [alt.capitalize() for alt in identity.alts]
    logger.debug(alts)
    alts_str = "{}, and {}".format(", ".join(alts[:-1]), alts[-1])

    if is_name(ident) and ident != identity.og:
        og_str = identity.og.capitalize()
        if identity.og.capitalize() in alts:
            alts.remove(og_str)
        alts_str = "{}, and {}".format(", ".join(alts[:-1]), alts[-1])
        msg = "They're normally known as {}, {}.".format(og_str, mention)
        msg = "{} They're also known as {}.".format(msg, alts_str)
        return msg

    return "I've heard they also go by {}.".format(alts_str)


def set_school(name, school, message, change=False):
    """
    TODO
    """
    mention = message.author.mention
    if not is_name(name):
        return "Sorry, {}, only Champions can use magic.".format(mention)

    if school not in schools:
        return "I'm not familiar with that school of magic."

    if name not in champions:
        return "I've not heard of a {} in the land of Aberoth..".format(
            name.capitalize()
        )

    champion = champions[name]

    if not change and champion.school != "":
        if champion.school == schools[school]:
            return "I think I already know that, {}".format(mention)

        return "{} {}, has this changed?".format(
            "I've been told that {} studies in the".format(name.capitalize()),
            "art of {}".format(champion.school.capitalize()))

    champion.school = schools[school]

    msg = "Thank you, {}, I'll keep that in mind.".format(mention)

    # TODO: Flavor text for each school.
    return msg


def champion_info(name, message):
    mention = message.author.mention
    if not is_name(name):
        return ("Sorry, {}, I can only tell you what I know about" +
                "citizens of Aberoth.").format(mention)

    if name not in champions:
        return "I've not heard of a {} in the land of Aberoth..".format(
            name.capitalize()
        )

    champion = champions[name]
    identity = None
    if champion.name in identities:
        identity = identities[champion.name]

    if identity is None:
        msg = "I haven't heard much about {}. However, I can tell you".format(
            name.capitalize())
    else:
        msg = "{} is also known by the name of".format(
            champion.name.capitalize())
        if int(identity.discord) != 0:
            msg = "{} <@!{}>.".format(msg, identity.discord)
        else:
            msg = "{} {}.".format(msg, identity.og.capitalize())

    # TODO: Also search champion for guild info
    if identity is not None and identity.guild != "":
        they = "they"
        if msg[-1] == ".":
            they = "They"
        msg = "{} {} are a member of {}".format(
            msg,
            they,
            " ".join([x.capitalize() for x in identity.guild.split()])
        )

        if champion.school != "":
            msg = "{}, and".format(msg)
        else:
            msg = "{}.".format(msg)

    if champion.school != "":
        they = "they"
        if msg[-1] == ".":
            they = "They"

        msg = "{} {} study in the school of {}.".format(
            msg, they, champion.school.capitalize())

    msg = "{} They have a skill total of {}.".format(msg, champion.skill_total)

    return msg
