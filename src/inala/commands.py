import re
import typing

import discord

from . import trivia

re_start_camp = re.compile(r'[A-z]+channel (for)? camps')

def parse_command(message) -> typing.Callable[[typing.Any], str]:
    if len(message) ==  1:
        return greeting
    elif len(content) == 2 and content[1].lower() in trivia:
        return get_trivia
    elif re_start_camp.match(" ".join(message[1:])):
        return start_camp
    else:
        return no_match


def no_match(message) -> str:
    no_match_message: str = "Greetings, {}. I do not know anything about that."
    return no_match_message.format(message.author.mention)


def greeting(message) -> str:
    # They only said our name.
    return "Greetings, {}. How can I help?".format(message.author.mention)


def get_trivia(message) -> str:
    return trivia[content[1].lower()].format(message.author.mention)


def generate_new_user(message) -> str:
    raise NotImplementedError


async def start_camp(message) -> str:
    """
    Posts a message to the channel and begins monitoring for reactions.
    """

    title = "{} Realm Camps"
    url = "https://tavelors.pub"

    embed: discord.Embed = discord.Embed(
        title=title,
        url=url,
    )

    await message.channel.send(embed)


async def start_mark(message) -> str:
    """
    Posts a message to the channel and begins monitoring for reactions.
    """

    msg: str = "React with what you've killed recently!"

    await message.channel.send(msg)
