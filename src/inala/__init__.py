"""
All modules for running a Inala Discord bot.
"""

import logging

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

from .commands import parse_command
